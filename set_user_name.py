#!/usr/bin/env python
"""
Set details about a user on the clock
"""
from __future__ import unicode_literals
import sys
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser
from argparse import ArgumentParser
import logging

import zkclock


logging.basicConfig(level=logging.INFO)

USHRT_MAX = 65535


if __name__ == "__main__":

    # IP address and port numbers for the timeclock we are connecting to
    parser = ConfigParser()
    parser.read('config.ini')
    host = parser.get('zkclock', 'host')
    port = parser.getint('zkclock', 'port')
    password = parser.get('zkclock', 'passcode')

    parser = ArgumentParser(description='Rename a user.')
    parser.add_argument('uid', type=int)
    parser.add_argument('name', type=str)
    args = parser.parse_args()
    user_id = args.uid
    if sys.version_info.major == 2:  # python 2
        if sys.stdin.encoding:
            user_name = args.name.decode(sys.stdin.encoding)
        else:
            user_name = args.name.decode('utf-8')
    else:
        user_name = args.name  # python 3

    s, r = zkclock.connect(host, port, password)
    r = zkclock.disable(s, r)

    r, userdata = zkclock.get_user_data(s, r)

    userdata = zkclock.combine_user_packets(userdata)
    # Create a dictionary of user id s to user names
    users = zkclock.unpack_users_to_dict(userdata)

    user = users[user_id]
    user.name = user_name
    userdata = zkclock.pack_user(user)

    r = zkclock.user_write(s, r, userdata)

    r = zkclock.enable(s, r)
    r = zkclock.disconnect(s, r)
