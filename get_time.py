#!/usr/bin/env python
"""
Get the time from the timeclock.
"""
from __future__ import unicode_literals
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser
import logging

import zkclock


logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":

    # IP address and port numbers for the timeclock we are connecting to
    parser = ConfigParser()
    parser.read('config.ini')
    host = parser.get('zkclock', 'host')
    port = parser.getint('zkclock', 'port')
    password = parser.get('zkclock', 'passcode')

    s, r = zkclock.connect(host, port, password)
    r = zkclock.disable(s, r)

    r, dt = zkclock.get_time(s, r)

    r = zkclock.enable(s, r)
    r = zkclock.disconnect(s, r)

    print(dt)
