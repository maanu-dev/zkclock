ZKClock
=======

Python code for connecting to and retrieving attendance log from time and attendance machines from ZK Software.

This was developed with a Ver 6.10 ZEM500 time and attendance machine. I no longer have access to this device so I can't test that this code works any better than it does. If you have something newer, have a look at the following:
 - https://bitbucket.org/johnmc/zkemapi
 - https://bitbucket.org/searchtrendseu/zem560


Requirements
------------

Install with pip::

   pip install -r requirements/python3.txt

Example scripts
---------------

First copy the config.ini.sample file to config.ini and change the details

 - get_time.py - gets the time from the clock
 - set_time.py - set the time on the clock
 - set_user_name.py - for setting usernames
 - get_attendance_log.py - get the attendance log from the clock and saves it to a pickle file for later processing
 - write_excel.py - run get_attendance_log.py first, writes attendance log as a spreadsheet

Run tests
---------

To run the tests use tox::
    tox


License
-------
Public domain or CC0 (as public domain doesn't exist everywhere).
