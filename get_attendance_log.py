#!/usr/bin/env python
"""
Get the user and attendance log data from the timeclock and
write it to a pickle file
"""
from __future__ import unicode_literals
import pickle
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser
import logging

import zkclock


logging.basicConfig(level=logging.INFO)

USHRT_MAX = 65535


if __name__ == "__main__":

    # IP address and port numbers for the timeclock we are connecting to
    parser = ConfigParser()
    parser.read('config.ini')
    host = parser.get('zkclock', 'host')
    port = parser.getint('zkclock', 'port')
    password = parser.get('zkclock', 'passcode')

    s, r = zkclock.connect(host, port, password)
    r = zkclock.disable(s, r)

    r, userdata = zkclock.get_user_data(s, r)

    userdata = zkclock.combine_user_packets(userdata)

    r, logdata = zkclock.get_log_data(s, r)

    r = zkclock.enable(s, r)
    r = zkclock.disconnect(s, r)

    # create a pickle file and save the data we downloaded to it
    with open('pickle-file', 'wb') as f:
        pickle.dump([userdata, logdata], f)
